package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.light.SpotLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import java.awt.Color;

public class Main extends SimpleApplication {
    private final String backgroudColorHex = "#A4AC86";
    private final String acornModelPath = "Models/Acorn/acorn.j3o";
    
    private Spatial model;
    
    private final float rotateStep = 0.1f;
    private float currentHorizontalAngle = 0f;
    private float currentVerticalAngle = 0f;
    
    private boolean isPlaying = false;
    private float rotateAnimationMultiplier = 0.5f;

    public static void main(String[] args) {
        Main app = new Main();
        
        AppSettings settings = new AppSettings(true);
        settings.setTitle("Lab4");
        app.setSettings(settings);
        
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
        setUpModel();
        setUpLight();
        setUpKeyboardListeners();
        setUpBackground();
    }

    @Override
    public void simpleUpdate(float tpf) {
        if (model == null || !isPlaying) {
            return;
        }
       
        model.rotate(
            rotateAnimationMultiplier * tpf, 
            -rotateAnimationMultiplier * tpf, 
            rotateAnimationMultiplier * tpf
        );
    }
    
    private void setUpModel() {
        model = assetManager.loadModel(acornModelPath);
        model.scale(100f, 100f, 100f);
        rootNode.attachChild(model);
    }
    
    private void setUpLight() {
        DirectionalLight sun = new DirectionalLight();
        sun.setColor(ColorRGBA.White);
        sun.setDirection(new Vector3f(-.5f,-.5f,-.5f).normalizeLocal());
        rootNode.addLight(sun);
        
        SpotLight spot = new SpotLight();
        spot.setSpotRange(100f);                           // distance
        spot.setSpotInnerAngle(15f * FastMath.DEG_TO_RAD); // inner light cone (central beam)
        spot.setSpotOuterAngle(35f * FastMath.DEG_TO_RAD); // outer light cone (edge of the light)
        spot.setColor(ColorRGBA.White.mult(1.3f));         // light color
        spot.setPosition(cam.getLocation());               // shine from camera loc
        spot.setDirection(cam.getDirection());             // shine forward from camera loc
        rootNode.addLight(spot);
    }
    
    private void setUpKeyboardListeners() {
        inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_DOWN));

        inputManager.addListener(pauseListener, "Pause");
        inputManager.addListener(rotateListener, "Left", "Right",  "Up", "Down");
    }
    
    private void setUpBackground() {
        Color color = Color.decode(backgroudColorHex);
        ColorRGBA colorRGBA = new ColorRGBA(
            (float) color.getRed() / 255, 
            (float) color.getGreen() / 255, 
            (float) color.getBlue() / 255, 
            1
        );
        viewPort.setBackgroundColor(colorRGBA);
    }
    
    private final ActionListener pauseListener = new ActionListener() {
        @Override
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (!keyPressed) {
                return;
            }
            
            if (name.equals("Pause")) {
                isPlaying = !isPlaying;
            }
        }
    };

    private final ActionListener rotateListener = new ActionListener() {
        @Override
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (model == null || !keyPressed) {
                return;
            }
            
            if (name.equals("Right")) {
                currentHorizontalAngle += rotateStep;
                model.rotate(currentHorizontalAngle, 0f, 0f);
            }
            if (name.equals("Left")) {
                currentHorizontalAngle -= rotateStep;
                model.rotate(currentHorizontalAngle, 0f, 0f);
            }
            if (name.equals("Up")) {
                currentVerticalAngle -= rotateStep;
                model.rotate(0f, 0f, currentVerticalAngle);
            }
            if (name.equals("Down")) {
                currentVerticalAngle -= rotateStep;
                model.rotate(0f, 0f, currentVerticalAngle);
            }
        }
    };
}
